package streamapi;

import java.util.*;

public class StreamAPIApp {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String[] words = text.split("[^0-9A-Za-zА-Яа-я]+");

        Map<String,Integer> map = new HashMap<>();

        Arrays.stream(words).
                forEach(word -> map.put(
                        word.toLowerCase(),
                        map.get(word.toLowerCase())==null?1:map.get(word.toLowerCase())+1)
                );

        map.entrySet().stream().
                sorted(Map.Entry.comparingByKey()).
                sorted((entry1,entry2)->entry2.getValue()-entry1.getValue()).
                limit(10).forEach(entry-> System.out.println(entry.getKey()));

    }
}